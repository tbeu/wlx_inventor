Autodesk® Inventor® Preview plugin 1.0.0.2 for Total Commander
==============================================================

 * License:
-----------

This software is released as freeware.


 * Disclaimer:
--------------

This software is provided "AS IS" without any warranty, either expressed or
implied, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose. The author will not be
liable for any special, incidental, consequential or indirect damages due to
loss of data or any other reason.


 * Installation:
----------------

Open the plugin archive using Total Commander and installation will start.
The default configuration file inventor.ini is initialized during the very first
plugin usage.


 * Description:
---------------

Autodesk Inventor stores a preview PNG for Inventor files.

The Autodesk Inventor Preview plugin shows the embedded preview thumbnail of
Inventor files. It can also be used for the thumbnail view of Total Commander >= 6.5.

Extracting the preview thumbnail does not require Autodesk Inventor to be
installed.

The default background color can be set in section [Autodesk Inventor Preview Settings]
of inventor.ini.

The optional status bar displays the Inventor Build information.


 * ChangeLog:
-------------

 o Version 1.0.0.2 (27.08.2019)
   - fixed resource leak during error handling
 o Version 1.0.0.1 (24.08.2019)
   - first build


 * References:
--------------

 o Accessing Thumbnail Images by Brian Ekins
   - https://modthemachine.typepad.com/my_weblog/2010/06/accessing-thumbnail-images.html
 o LS-Plugin Writer's Guide by Christian Ghisler
   - http://ghisler.fileburst.com/lsplugins/listplughelp2.1.zip


 * Trademark and Copyright Statements:
--------------------------------------

 o Autodesk and Inventor are registered trademarks or trademarks of Autodesk,
   Inc./Autodesk Canada Co.
   - http://www.autodesk.com/inventor
 o Total Commander is Copyright © 1993-2019 by Christian Ghisler, Ghisler Software GmbH.
   - http://www.ghisler.com


 * Feedback:
------------

If you have problems, questions, suggestions please contact Thomas Beutlich.
 o Email: support@tbeu.de
 o URL: http://tbeu.totalcmd.net