// wlx_inventor.cpp : Definiert die exportierten Funktionen für die DLL-Anwendung.
//

#include "stdafx.h"
#include "cunicode.h"
#include "listplug.h" // WLX
#include "resource.h"

#include <comdef.h>
#include <commctrl.h>
#include <GdiPlus.h>
#include <InitGuid.h>
#include <array>
#include <string>

// helper function declarations
static void DisplayLastCOMErrorMsgW(HRESULT hr, const wchar_t* fcnName, const wchar_t* fileName, UINT msgType);
static HBITMAP CopyBitmap(HBITMAP hBitmapSrc);
static BOOL GetFileVersionW(const wchar_t* fileName, wchar_t* verStr, size_t maxSize);
static HBITMAP GetPreviewBitmapW(const wchar_t* fileName);

// window function declarations
static LRESULT CALLBACK WndProcMain(HWND, UINT, WPARAM, LPARAM);
static LRESULT CALLBACK WndProcView(HWND, UINT, WPARAM, LPARAM);

// defines
#define _detectstring "EXT=\"IAM\"|EXT=\"IDE\"|EXT=\"IDW\"|EXT=\"IPN\"|EXT=\"IPT\""
#define PLUGIN_NAME L"Autodesk Inventor Preview"
#define PLUGIN L"Autodesk Inventor Preview 1.0.0.2"
#define SETTINGS L"Autodesk Inventor Preview Settings"
#define SET_DEBUG GetPrivateProfileIntW(SETTINGS, L"Debug", 0, iniFileName)
#define VERSTRMAXLENGTH 128
#define WM_SET_SCROLLBAR (WM_APP + 101)
#define SCROLL_LINE 30
#ifndef WM_MOUSEWHEEL
#define WM_MOUSEWHEEL 0x020A
#endif

// {3D38DE39-0588-4c14-BB37-18F4D5DD31C7}
constexpr GUID FMTID_SummaryInformation_Inventor{ 0x3d38de39, 0x0588, 0x4c14, 0xbb, 0x37, 0x18, 0xf4, 0xd5, 0xdd, 0x31, 0xc7 };
// {32853F0F-3444-11D1-9E93-0060B03C1CA6}
constexpr GUID FMTID_DesignTracking_Inventor{ 0x32853f0f, 0x3444, 0x11d1, 0x9e, 0x93, 0x00, 0x60, 0xb0, 0x3c, 0x1c, 0xa6 };
constexpr PROPID PIDDT_VER{ 67 };

struct PACKEDMETA
{
	WORD mm;
	WORD xExt;
	WORD yExt;
	WORD dummy;
};

typedef struct
{
	wchar_t fileName[wdirtypemax];
	wchar_t version[VERSTRMAXLENGTH];
	HBITMAP hBitmap;
	int width;
	int height;
	BOOL hScroll;
	BOOL vScroll;
	BOOL centerImage;
	BOOL fit2Window;
	BOOL fitLargerOnly;
	HWND hWndStatus;
	HWND hWndView;
	HBRUSH hBrush;
	COLORREF bgColor;
} WNDVAR;

// global variables
HINSTANCE hinst{ nullptr };
static wchar_t iniFileName[wdirtypemax]{};

static char* strlcpy(char* dst, const char* src, size_t maxSize)
{
	if (strlen(src) > maxSize)
	{
		strncpy(dst, src, maxSize - 1);
		dst[maxSize - 1] = 0;
	}
	else
	{
		strcpy(dst, src);
	}
	return dst;
}

static char* strlcat(char* dst, const char* src, size_t maxSize)
{
	size_t len = strlen(dst);
	if (strlen(src) + 1 + len >= maxSize)
	{
		strncat(dst, src, maxSize - 1 - len);
		dst[maxSize - 1] = 0;
	}
	else
	{
		strcat(dst, src);
	}
	return dst;
}

static void DisplayLastCOMErrorMsgW(HRESULT hr, const wchar_t* fcnName, const wchar_t* fileName, UINT msgType)
{
	_com_error error(hr);
	const wchar_t* msgBuf = error.ErrorMessage();
	std::wstring strErrorText = std::wstring(msgBuf);
	if (fileName)
	{
		size_t pos = strErrorText.find(L"%1");
		if (pos != std::wstring::npos)
		{
			strErrorText.replace(pos, wcslen(L"%1"), fileName);
		}
	}
	// display the error message
	size_t s = strErrorText.length() + wcslen(fcnName) + 100;
	wchar_t* displayBuf = new wchar_t[s + 1];
	if (displayBuf)
	{
		ZeroMemory(displayBuf, (s + 1)*(sizeof(wchar_t)));
		_snwprintf(displayBuf, s, L"%s failed with error %x: %s", fcnName, hr, strErrorText.c_str());
		MessageBoxW(nullptr, displayBuf, PLUGIN, msgType);
		delete[] displayBuf;
	}
}

static HBITMAP CopyBitmap(HBITMAP hBitmapSrc)
{
	BITMAP bm;
	ZeroMemory(&bm, sizeof(BITMAP));
	GetObjectW(hBitmapSrc, sizeof(BITMAP), &bm);
	HBITMAP hBitmapDst = CreateBitmapIndirect(&bm);
	HDC hdcSrc = CreateCompatibleDC(nullptr);
	HDC hdcDst = CreateCompatibleDC(nullptr);
	SelectObject(hdcSrc, hBitmapSrc);
	SelectObject(hdcDst, hBitmapDst);
	BitBlt(hdcDst, 0, 0, bm.bmWidth, bm.bmHeight, hdcSrc, 0, 0, SRCCOPY);
	DeleteDC(hdcSrc);
	DeleteDC(hdcDst);
	return hBitmapDst;
}

static BOOL GetFileVersionW(const wchar_t* fileName, wchar_t* verStr, size_t maxSize)
{
	BOOL verFound{ FALSE };
	LPSTORAGE pIStorage{ nullptr };

	auto hr = StgOpenStorageEx(fileName, STGM_READ | STGM_SHARE_DENY_WRITE, STGFMT_ANY, 0, nullptr, nullptr, IID_IPropertySetStorage, reinterpret_cast<void**>(&pIStorage));
	if (SUCCEEDED(hr) && pIStorage)
	{
		IPropertySetStorage* pPropertySetStorage{ nullptr };
		// get the Storage interface
		hr = pIStorage->QueryInterface(IID_IPropertySetStorage, reinterpret_cast<void**>(&pPropertySetStorage));
		if (SUCCEEDED(hr) && pPropertySetStorage)
		{
			IPropertyStorage* pPropertyStorage{ nullptr };
			// open up Inventor design tracking properties
			hr = pPropertySetStorage->Open(FMTID_DesignTracking_Inventor, STGM_READ | STGM_SHARE_EXCLUSIVE, &pPropertyStorage);
			if (SUCCEEDED(hr) && pPropertyStorage)
			{
				PROPSPEC propSpec{};
				PROPVARIANT propVar{};
				propSpec.ulKind = PRSPEC_PROPID;
				propSpec.propid = PIDDT_VER;
				hr = pPropertyStorage->ReadMultiple(1, &propSpec, &propVar);
				if (SUCCEEDED(hr))
				{
					if (propVar.vt == VT_LPWSTR)
					{
						wcslcpy(verStr, propVar.pwszVal, min(static_cast<int>(maxSize), static_cast<int>(wcslen(propVar.pwszVal) + 1)));
						verFound = TRUE;
					}
					else if (SET_DEBUG)
					{
						DisplayLastCOMErrorMsgW(HRESULT_FROM_WIN32(ERROR_INVALID_VARIANT), L"IPropertySetStorage::ReadMultiple", L"Design Tracking Properties", MB_OK | MB_ICONSTOP);
					}
				}
				else if (FAILED(hr) && SET_DEBUG)
				{
					DisplayLastCOMErrorMsgW(hr, L"IPropertySetStorage::ReadMultiple", L"Design Tracking Properties", MB_OK | MB_ICONSTOP);
				}
				PropVariantClear(&propVar);
				pPropertySetStorage->Delete(FMTID_DesignTracking_Inventor);
			}
			else if (FAILED(hr) && SET_DEBUG)
			{
				DisplayLastCOMErrorMsgW(hr, L"IPropertySetStorage::Open", L"Design Tracking Properties", MB_OK | MB_ICONSTOP);
			}
			pPropertySetStorage->Release();
		}
		else if (FAILED(hr) && SET_DEBUG)
		{
			DisplayLastCOMErrorMsgW(hr, L"LPSTORAGE::QueryInterface", nullptr, MB_OK | MB_ICONSTOP);
		}
		pIStorage->Release();
	}
	else if (FAILED(hr) && SET_DEBUG)
	{
		DisplayLastCOMErrorMsgW(hr, L"StgOpenStorage", fileName, MB_OK | MB_ICONSTOP);
	}

	return verFound;
}

static HBITMAP GetBitmapFromPNG(const void* pv, ULONG nSize)
{
	HBITMAP hBitmap{ nullptr };
	LPSTREAM pngStream{ nullptr };
	auto hr = ::CreateStreamOnHGlobal(nullptr, TRUE, &pngStream);
	if (SUCCEEDED(hr) && pngStream)
	{
		hr = pngStream->Write(pv, nSize, nullptr);
		if (SUCCEEDED(hr))
		{
			Gdiplus::GdiplusStartupInput gdiplusStartupInput{};
			ULONG_PTR gdiplusToken{};
			Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
			{
				auto bitmap = Gdiplus::Bitmap::FromStream(pngStream);
				Gdiplus::Color color{};
				color.SetFromCOLORREF(GetPrivateProfileIntW(SETTINGS, L"BackColor", RGB(255, 255, 255), iniFileName));
				bitmap->GetHBITMAP(color, &hBitmap);
				pngStream->Release();
				delete bitmap;
			}
			Gdiplus::GdiplusShutdown(gdiplusToken);
		}
	}
	return hBitmap;
}

static HBITMAP GetBitmapFromMetaFile(const BYTE* pb, UINT nSize, WORD xExt, WORD yExt)
{
	HBITMAP hBitmap{ nullptr };
	const ULONG nSizeImage{ 4UL * xExt * yExt };
	auto buffer = std::make_unique<BYTE[]>(sizeof(BITMAPINFO) + nSizeImage);
	if (auto lpbmi = reinterpret_cast<BITMAPINFO*>(buffer.get()))
	{
		if (auto hMetaFile = SetMetaFileBitsEx(nSize, pb))
		{
			ZeroMemory(lpbmi, sizeof(BITMAPINFOHEADER));
			lpbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
			lpbmi->bmiHeader.biWidth = xExt;
			lpbmi->bmiHeader.biHeight = yExt;
			lpbmi->bmiHeader.biPlanes = 1;
			lpbmi->bmiHeader.biBitCount = 32;
			lpbmi->bmiHeader.biCompression = BI_RGB;
			lpbmi->bmiHeader.biSizeImage = 0;

			auto hWnd = GetDesktopWindow();
			if (auto hDC = GetDC(hWnd))
			{
				void* bits{ nullptr };
				hBitmap = CreateDIBSection(hDC, lpbmi, DIB_RGB_COLORS, &bits, nullptr, 0);
				GdiFlush();

				auto hDCDst = CreateCompatibleDC(hDC);
				auto hDCSrc = CreateCompatibleDC(hDC);
				auto hBitmapScaled = CreateCompatibleBitmap(hDC, xExt, yExt);
				ReleaseDC(hWnd, hDC);
				auto hOldBitmap = (HBITMAP)SelectObject(hDCSrc, hBitmap);
				PlayMetaFile(hDCSrc, hMetaFile);
				auto hOldBitmapScaled = (HBITMAP)SelectObject(hDCDst, hBitmapScaled);
				BitBlt(hDCDst, 0, 0, xExt, yExt, hDCSrc, 0, 0, SRCCOPY);
				SelectObject(hDCSrc, hOldBitmap);
				SelectObject(hDCDst, hOldBitmapScaled);
				DeleteDC(hDCSrc);
				DeleteDC(hDCDst);
				DeleteObject(hBitmap);
				hBitmap = hBitmapScaled;
			}
		}
	}
	return hBitmap;
}

static HBITMAP GetPreviewBitmapW(const wchar_t* fileName)
{
	HBITMAP hBitmap{ nullptr };
	LPSTORAGE pIStorage{ nullptr };

	auto hr = StgOpenStorageEx(fileName, STGM_READ | STGM_SHARE_DENY_WRITE, STGFMT_ANY, 0, nullptr, nullptr, IID_IPropertySetStorage, reinterpret_cast<void**>(&pIStorage));
	if (SUCCEEDED(hr) && pIStorage)
	{
		IPropertySetStorage* pPropertySetStorage{ nullptr };
		// get the Storage interface
		hr = pIStorage->QueryInterface(IID_IPropertySetStorage, reinterpret_cast<void**>(&pPropertySetStorage));
		if (SUCCEEDED(hr) && pPropertySetStorage)
		{
			IPropertyStorage* pPropertyStorage{ nullptr };
			// open up Inventor summary information
			hr = pPropertySetStorage->Open(FMTID_SummaryInformation_Inventor, STGM_READ | STGM_SHARE_EXCLUSIVE, &pPropertyStorage);
			if (SUCCEEDED(hr) && pPropertyStorage)
			{
				PROPSPEC propSpec{};
				PROPVARIANT propVar{};
				propSpec.ulKind = PRSPEC_PROPID;
				propSpec.propid = PIDSI_THUMBNAIL;
				hr = pPropertyStorage->ReadMultiple(1, &propSpec, &propVar);
				if (SUCCEEDED(hr))
				{
					if (propVar.vt == VT_CF)
					{
						// got clipboard format
						const auto pClip = propVar.pclipdata;
						if (pClip && pClip->ulClipFmt == -1 && pClip->cbSize > 0)
						{
							// got built-in Windows format
							auto pb = pClip->pClipData;
							if (pb && *reinterpret_cast<DWORD*>(pb) == CF_METAFILEPICT)
							{
								// got metafile format
								constexpr BYTE fmtSize{ sizeof(DWORD) };
								pb += fmtSize; // Skip the clip type
								constexpr BYTE headerSize{ sizeof(PACKEDMETA) };
								const auto pPackedMeta = reinterpret_cast<PACKEDMETA*>(pb);
								pb += sizeof(PACKEDMETA); // Skip packed meta struct

								constexpr std::array<BYTE, 8> pngSignature{ 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A };
								if (0 == memcmp(pngSignature.data(), pb, pngSignature.size()))
								{
									// got png format
									const ULONG nSize{ pClip->cbSize - fmtSize - headerSize };
									hBitmap = GetBitmapFromPNG(pb, nSize);
								}
								else
								{
									const UINT nSize{ pClip->cbSize - sizeof(pClip->ulClipFmt) - fmtSize - headerSize };
									hBitmap = GetBitmapFromMetaFile(pb, nSize, pPackedMeta->xExt, pPackedMeta->yExt);
								}
							}
							else if (pb && SET_DEBUG)
							{
								DisplayLastCOMErrorMsgW(HRESULT_FROM_WIN32(ERROR_METAFILE_NOT_SUPPORTED), L"IPropertySetStorage::ReadMultiple", L"Inventor Summary Information", MB_OK | MB_ICONSTOP);
							}
						}
						else if (pClip && pClip->ulClipFmt != -1 && SET_DEBUG)
						{
							DisplayLastCOMErrorMsgW(DV_E_CLIPFORMAT, L"IPropertySetStorage::ReadMultiple", L"Inventor Summary Information", MB_OK | MB_ICONSTOP);
						}
					}
					else if (SET_DEBUG)
					{
						DisplayLastCOMErrorMsgW(HRESULT_FROM_WIN32(ERROR_INVALID_VARIANT), L"IPropertySetStorage::ReadMultiple", L"Inventor Summary Information", MB_OK | MB_ICONSTOP);
					}
				}
				else if (FAILED(hr) && SET_DEBUG)
				{
					DisplayLastCOMErrorMsgW(hr, L"IPropertySetStorage::ReadMultiple", L"Inventor Summary Information", MB_OK | MB_ICONSTOP);
				}
				PropVariantClear(&propVar);
				pPropertySetStorage->Delete(FMTID_SummaryInformation_Inventor);
			}
			else if (FAILED(hr) && SET_DEBUG)
			{
				DisplayLastCOMErrorMsgW(hr, L"IPropertySetStorage::Open", L"Inventor Summary Information", MB_OK | MB_ICONSTOP);
			}
			pPropertySetStorage->Release();
		}
		else if (FAILED(hr) && SET_DEBUG)
		{
			DisplayLastCOMErrorMsgW(hr, L"LPSTORAGE::QueryInterface", nullptr, MB_OK | MB_ICONSTOP);
		}
		pIStorage->Release();
	}
	else if (FAILED(hr) && SET_DEBUG)
	{
		DisplayLastCOMErrorMsgW(hr, L"StgOpenStorage", fileName, MB_OK | MB_ICONSTOP);
	}

	return hBitmap;
}

static LRESULT CALLBACK WndProcMain(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
	{
		auto hWndVar = static_cast<WNDVAR*>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams);
		SetWindowLongPtrW(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(hWndVar));
		if (hWndVar)
		{
			if (GetPrivateProfileIntW(SETTINGS, L"StatusBar", 1, iniFileName))
			{
				InitCommonControls();

				DWORD dwStyle = GetWindowLongW(GetParent(hWnd), GWL_STYLE);
				if (dwStyle & WS_CHILD)
				{
					// Quick View
					dwStyle = WS_CHILD | WS_VISIBLE;
				}
				else
				{
					dwStyle = WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP;
				}
				hWndVar->hWndStatus = CreateWindowExW(0, STATUSCLASSNAMEW, L"", dwStyle, 0, 0, 0, 0, hWnd, nullptr, hinst, nullptr);
				if (hWndVar->hWndStatus)
				{
					int aWidths[1]{ -1 };
					SendMessageW(hWndVar->hWndStatus, SB_SETPARTS, (WPARAM)1, (LPARAM)aWidths);
					SendMessageW(hWndVar->hWndStatus, SB_SIMPLE, (WPARAM)FALSE, 0);

					// update status bar message
					if (GetFileVersionW(hWndVar->fileName, hWndVar->version, VERSTRMAXLENGTH))
					{
						SendMessageW(hWndVar->hWndStatus, SB_SETTEXT, (WPARAM)0, (LPARAM)hWndVar->version);
					}
				}
			}

			static wchar_t szName[]{ L"BitmapView" };
			WNDCLASSEXW WinClass;
			ZeroMemory(&WinClass, sizeof(WNDCLASSEXW));
			WinClass.style = CS_HREDRAW | CS_VREDRAW;
			WinClass.lpfnWndProc = static_cast<WNDPROC>(WndProcView);
			WinClass.cbClsExtra = 0;
			WinClass.cbWndExtra = DLGWINDOWEXTRA;
			WinClass.hInstance = hinst;
			WinClass.hIcon = LoadIconW(nullptr, MAKEINTRESOURCEW(32512));
			WinClass.hCursor = LoadCursorW(nullptr, MAKEINTRESOURCEW(32512));
			WinClass.lpszClassName = szName;
			WinClass.cbSize = sizeof(WNDCLASSEXW);
			WinClass.hIconSm = LoadIconW(nullptr, MAKEINTRESOURCEW(32512));

			RegisterClassExW(&WinClass);

			RECT clientArea;
			ZeroMemory(&clientArea, sizeof(RECT));
			GetClientRect(GetParent(hWnd), &clientArea);

			if (hWndVar->hWndStatus)
			{
				RECT statusArea;
				ZeroMemory(&statusArea, sizeof(RECT));
				GetWindowRect(hWndVar->hWndStatus, &statusArea);

				hWndVar->hWndView = CreateWindowExW(0, szName, L"", WS_CHILD | WS_VISIBLE, clientArea.left, clientArea.top, clientArea.right - clientArea.left, clientArea.bottom - (statusArea.bottom - statusArea.top) - clientArea.top, hWnd, nullptr, hinst, (LPVOID)hWndVar);
			}
			else
			{
				hWndVar->hWndView = CreateWindowExW(0, szName, L"", WS_CHILD | WS_VISIBLE, clientArea.left, clientArea.top, clientArea.right - clientArea.left, clientArea.bottom - clientArea.top, hWnd, nullptr, hinst, (LPVOID)hWndVar);
			}
		}
		break;
	}

	case WM_ERASEBKGND:
		return 1;

	case WM_SIZE:
	{
		auto hWndVar = reinterpret_cast<WNDVAR*>(GetWindowLongPtrW(hWnd, GWLP_USERDATA));
		if (hWndVar && hWndVar->hWndStatus)
		{
			// Auto-resize statusbar (Send WM_SIZE message does just that)
			SendMessageW(hWndVar->hWndStatus, WM_SIZE, 0, 0);
		}
		if (hWndVar && hWndVar->hWndView)
		{
			if (hWndVar->hWndStatus)
			{
				RECT statusArea;
				ZeroMemory(&statusArea, sizeof(RECT));
				GetWindowRect(hWndVar->hWndStatus, &statusArea);
				SendMessageW(hWndVar->hWndView, WM_SIZE, wParam, MAKELONG(LOWORD(lParam), HIWORD(lParam) - (statusArea.bottom - statusArea.top)));
			}
			else
			{
				SendMessageW(hWndVar->hWndView, WM_SIZE, wParam, lParam);
			}
		}
		//SendMessageW(hWndVar->hWndView, WM_SET_SCROLLBAR, 0, 0);
		InvalidateRect(hWndVar->hWndView, nullptr, FALSE);
		break;
	}

	case WM_DESTROY:
	{
		auto hWndVar = reinterpret_cast<WNDVAR*>(GetWindowLongPtrW(hWnd, GWLP_USERDATA));
		if (hWndVar)
		{
			if (hWndVar->hWndStatus)
			{
				DestroyWindow(hWndVar->hWndStatus);
				hWndVar->hWndStatus = nullptr;
			}
			if (hWndVar->hWndView)
			{
				DestroyWindow(hWndVar->hWndView);
				hWndVar->hWndView = nullptr;
			}
		}
		break;
	}

	default:
		return DefWindowProcW(hWnd, message, wParam, lParam);
	}
	return 0;
}

static LRESULT CALLBACK WndProcView(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
	{
		auto hWndVar = static_cast<WNDVAR*>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams);
		SetWindowLongPtrW(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(hWndVar));
		if (hWndVar)
		{
			hWndVar->hBrush = CreateSolidBrush(hWndVar->bgColor);
			if (!hWndVar->hBitmap)
			{
				hWndVar->hBitmap = GetPreviewBitmapW(hWndVar->fileName);
			}
		}
		break;
	}

	case WM_PAINT:
	{
		auto hWndVar = reinterpret_cast<WNDVAR*>(GetWindowLongPtrW(hWnd, GWLP_USERDATA));

		PAINTSTRUCT ps;
		HDC hDC = BeginPaint(hWnd, &ps);
		RECT r;
		GetClientRect(hWnd, &r);
		FillRect(ps.hdc, &r, hWndVar->hBrush);
		HDC hdcMem = CreateCompatibleDC(hDC);
		HBITMAP hbmOld = (HBITMAP)SelectObject(hdcMem, hWndVar->hBitmap);
		BITMAP bm;
		ZeroMemory(&bm, sizeof(BITMAP));
		if (GetObjectW(hWndVar->hBitmap, sizeof(bm), &bm))
		{
			int xSrc = r.left;
			int xDst = r.left;
			int ySrc = r.top;
			int yDst = r.top;

			if (hWndVar->hScroll)
			{
				xSrc += GetScrollPos(hWnd, SB_HORZ);
			}
			if (hWndVar->vScroll)
			{
				ySrc += GetScrollPos(hWnd, SB_VERT);
			}
			if (hWndVar->fit2Window)
			{
				int wSrc = bm.bmWidth;
				int hSrc = bm.bmHeight;
				int wDst = r.right;
				int hDst = r.bottom;
				// do we need to stretch?
				if ((wSrc >= wDst || hSrc >= hDst) || !hWndVar->fitLargerOnly)
				{
					int stretchy = MulDiv(wDst, hSrc, wSrc);
					if (stretchy <= hDst)
					{
						hDst = stretchy;
						if (hDst < 1)
						{
							hDst = 1;
						}
					}
					else
					{
						int stretchx = MulDiv(hDst, wSrc, hSrc);
						wDst = stretchx;
						if (wDst < 1)
						{
							wDst = 1;
						}
					}
				}
				else
				{
					wDst = wSrc;
					hDst = hSrc;
				}

				if (hWndVar->centerImage)
				{
					if (!hWndVar->hScroll)
					{
						xDst = (r.right - wDst) / 2;
						xSrc = 0;
					}
					if (!hWndVar->vScroll)
					{
						yDst = (r.bottom - hDst) / 2;
						ySrc = 0;
					}
				}

				SetStretchBltMode(hDC, HALFTONE);
				POINT pt;
				SetBrushOrgEx(hDC, 0, 0, &pt);
				StretchBlt(hDC, xDst, yDst, wDst, hDst, hdcMem, xSrc, ySrc, wSrc, hSrc, SRCCOPY);
				hWndVar->width = wDst;
				hWndVar->height = hDst;
			}
			else
			{
				int wDst = bm.bmWidth;
				int hDst = bm.bmHeight;
				if (hWndVar->centerImage)
				{
					if (!hWndVar->hScroll)
					{
						xDst = (r.right - wDst) / 2;
						xSrc = 0;
					}
					if (!hWndVar->vScroll)
					{
						yDst = (r.bottom - hDst) / 2;
						ySrc = 0;
					}
				}

				BitBlt(hDC, xDst, yDst, wDst, hDst, hdcMem, xSrc, ySrc, SRCCOPY);
				hWndVar->width = wDst;
				hWndVar->height = hDst;
			}

		}
		SelectObject(hdcMem, hbmOld);
		DeleteDC(hdcMem);
		EndPaint(hWnd, &ps);
		break;
	}

	case WM_ERASEBKGND:
		return 1;

	case WM_SIZE:
	{
		int w = LOWORD(lParam);
		int h = HIWORD(lParam);
		RECT r;
		GetClientRect(hWnd, &r);
		if (r.bottom != h || r.right != w)
		{
			MoveWindow(hWnd, 0, 0, w, h, TRUE);
		}

		auto hWndVar = reinterpret_cast<WNDVAR*>(GetWindowLongPtrW(hWnd, GWLP_USERDATA));
		if (hWndVar && hWndVar->hBitmap)
		{
			SendMessageW(hWnd, WM_SET_SCROLLBAR, 0, 0);
			InvalidateRect(hWnd, nullptr, FALSE);
		}
		break;
	}

	case WM_SET_SCROLLBAR:
	{
		auto hWndVar = reinterpret_cast<WNDVAR*>(GetWindowLongPtrW(hWnd, GWLP_USERDATA));
		if (hWndVar && (hWndVar->width == 0) && (hWndVar->height == 0) && hWndVar->fit2Window)
		{
			// no scrollbar in very first Quick-View
			break;
		}

		BITMAP bm;
		ZeroMemory(&bm, sizeof(BITMAP));
		if (GetObjectW(hWndVar->hBitmap, sizeof(BITMAP), &bm))
		{
			RECT r;
			GetClientRect(hWnd, &r);
			if ((r.right - r.left < bm.bmWidth - 1) && !hWndVar->fit2Window)
			{
				int pos = GetScrollPos(hWnd, SB_HORZ);
				pos = (pos < bm.bmWidth - r.right - r.left) ? pos : bm.bmWidth - r.right - r.left;

				SCROLLINFO si;
				ZeroMemory(&si, sizeof(SCROLLINFO));
				si.cbSize = sizeof(SCROLLINFO);
				si.fMask = SIF_POS | SIF_PAGE | SIF_RANGE;
				si.nMin = 0;
				si.nMax = bm.bmWidth - 1;
				si.nPage = r.right - r.left;
				si.nPos = pos;
				si.nTrackPos = 0;
				SetScrollInfo(hWnd, SB_HORZ, &si, TRUE);
				ShowScrollBar(hWnd, SB_HORZ, TRUE);
				hWndVar->hScroll = TRUE;
			}
			else
			{
				ShowScrollBar(hWnd, SB_HORZ, FALSE);
				hWndVar->hScroll = FALSE;
			}

			GetClientRect(hWnd, &r);
			if ((r.bottom - r.top < bm.bmHeight - 1) && !hWndVar->fit2Window)
			{
				int pos = GetScrollPos(hWnd, SB_VERT);
				pos = (pos < bm.bmHeight - r.bottom - r.top) ? pos : bm.bmHeight - r.bottom - r.top;

				SCROLLINFO si;
				ZeroMemory(&si, sizeof(SCROLLINFO));
				si.cbSize = sizeof(SCROLLINFO);
				si.fMask = SIF_POS | SIF_PAGE | SIF_RANGE;
				si.nMin = 0;
				si.nMax = bm.bmHeight - 1;
				si.nPage = r.bottom - r.top;
				si.nPos = pos;
				si.nTrackPos = 0;
				SetScrollInfo(hWnd, SB_VERT, &si, TRUE);
				ShowScrollBar(hWnd, SB_VERT, TRUE);
				hWndVar->vScroll = TRUE;
			}
			else
			{
				ShowScrollBar(hWnd, SB_VERT, FALSE);
				hWndVar->vScroll = FALSE;
			}
		}
		break;
	}

	case WM_HSCROLL:
	{
		int max, min, pos, i;

		GetScrollRange(hWnd, SB_HORZ, &min, &max);
		if (max == 1)
		{
			break;
		}
		i = pos = GetScrollPos(hWnd, SB_HORZ);
		RECT r;
		GetClientRect(hWnd, &r);

		switch (LOWORD(wParam))
		{
		case SB_TOP:
			pos = 0;
			break;

		case SB_BOTTOM:
			pos = max;
			break;

		case SB_LINELEFT:
			pos -= SCROLL_LINE;
			break;

		case SB_LINERIGHT:
			pos += SCROLL_LINE;
			break;

		case SB_PAGELEFT:
			pos -= r.right;
			break;

		case SB_PAGERIGHT:
			pos += r.right;
			break;

		case SB_THUMBPOSITION:
		case SB_THUMBTRACK:
		{
			SCROLLINFO si;
			ZeroMemory(&si, sizeof(SCROLLINFO));
			si.cbSize = sizeof(SCROLLINFO);
			si.fMask = SIF_ALL;
			GetScrollInfo(hWnd, SB_HORZ, &si);
			pos = si.nTrackPos;
			break;
		}
		}
		SetScrollPos(hWnd, SB_HORZ, pos, TRUE);
		pos = GetScrollPos(hWnd, SB_HORZ);
		ScrollWindowEx(hWnd, i - pos, 0, nullptr, &r, nullptr, nullptr, SW_INVALIDATE | SW_ERASE);
		break;
	}

	case WM_VSCROLL:
	{
		int max, min, pos, i;

		GetScrollRange(hWnd, SB_VERT, &min, &max);
		if (max == 1)
		{
			break;
		}
		i = pos = GetScrollPos(hWnd, SB_VERT);
		RECT r;
		GetClientRect(hWnd, &r);

		switch (LOWORD(wParam))
		{
		case SB_TOP:
			pos = 0;
			break;

		case SB_BOTTOM:
			pos = max;
			break;

		case SB_LINEUP:
			pos -= SCROLL_LINE;
			break;

		case SB_LINEDOWN:
			pos += SCROLL_LINE;
			break;

		case SB_PAGEUP:
			pos -= r.bottom;
			break;

		case SB_PAGEDOWN:
			pos += r.bottom;
			break;

		case SB_THUMBPOSITION:
		case SB_THUMBTRACK:
		{
			SCROLLINFO si;
			ZeroMemory(&si, sizeof(SCROLLINFO));
			si.cbSize = sizeof(SCROLLINFO);
			si.fMask = SIF_ALL;
			GetScrollInfo(hWnd, SB_VERT, &si);
			pos = si.nTrackPos;
			break;
		}
		}
		SetScrollPos(hWnd, SB_VERT, pos, TRUE);

		pos = GetScrollPos(hWnd, SB_VERT);
		ScrollWindowEx(hWnd, 0, i - pos, nullptr, &r, nullptr, nullptr, SW_INVALIDATE | SW_ERASE);
		break;
	}

	case WM_MOUSEWHEEL:
	{
		auto hWndVar = reinterpret_cast<WNDVAR*>(GetWindowLongPtrW(hWnd, GWLP_USERDATA));
		if (hWndVar && hWndVar->vScroll)
		{
			for (int i = 0; i < 3; i++)
			{
				SendMessageW(hWnd, WM_VSCROLL, ((short)HIWORD(wParam) > 0) ? SB_LINEUP : SB_LINEDOWN, 0);
			}
		}
		break;
	}

	case WM_DESTROY:
	{
		auto hWndVar = reinterpret_cast<WNDVAR*>(GetWindowLongPtrW(hWnd, GWLP_USERDATA));
		if (hWndVar)
		{
			if (hWndVar->hBrush)
			{
				DeleteObject(hWndVar->hBrush);
				hWndVar->hBrush = nullptr;
			}
			if (hWndVar->hBitmap)
			{
				DeleteObject(hWndVar->hBitmap);
				hWndVar->hBitmap = nullptr;
			}
		}
		break;
	}

	default:
		return DefWindowProcW(hWnd, message, wParam, lParam);
	}
	return 0;
}

void __stdcall ListGetDetectString(char* DetectString, int maxLen)
{
	strlcpy(DetectString, _detectstring, maxLen + 1);
}

HWND __stdcall ListLoad(HWND ParentWin, char* FileToLoad, int ShowFlags)
{
	wchar_t FileToLoadW[wdirtypemax];
	return ListLoadW(ParentWin, awfilenamecopy(FileToLoadW, FileToLoad), ShowFlags);
}

HWND __stdcall ListLoadW(HWND ParentWin, wchar_t* FileToLoad, int ShowFlags)
{
	HWND hWnd = nullptr;
	WNDCLASSEXW WinClass;
	ZeroMemory(&WinClass, sizeof(WNDCLASSEXW));
	WinClass.style = CS_HREDRAW | CS_VREDRAW;
	WinClass.lpfnWndProc = static_cast<WNDPROC>(WndProcMain);
	WinClass.cbClsExtra = 0;
	WinClass.cbWndExtra = DLGWINDOWEXTRA;
	WinClass.hInstance = hinst;
	WinClass.hIcon = LoadIconW(nullptr, MAKEINTRESOURCEW(32512));
	WinClass.hCursor = LoadCursorW(nullptr, MAKEINTRESOURCEW(32512));
	WinClass.lpszClassName = PLUGIN_NAME;
	WinClass.cbSize = sizeof(WNDCLASSEXW);
	WinClass.hIconSm = LoadIconW(nullptr, MAKEINTRESOURCEW(32512));

	RegisterClassExW(&WinClass);

	RECT r;
	GetClientRect(ParentWin, &r);

	WNDVAR* hWndVar = new WNDVAR;
	if (!hWndVar)
	{
		return hWnd;
	}

	ZeroMemory(hWndVar, sizeof(WNDVAR));
	wcslcpy(hWndVar->fileName, FileToLoad, wdirtypemax);
	hWndVar->fit2Window = (ShowFlags & lcp_fittowindow) ? TRUE : FALSE;
	hWndVar->fitLargerOnly = (ShowFlags & lcp_fitlargeronly) ? TRUE : FALSE;
	hWndVar->hScroll = FALSE;
	hWndVar->vScroll = FALSE;
	hWndVar->width = 0;
	hWndVar->height = 0;
	hWndVar->bgColor = GetPrivateProfileIntW(SETTINGS, L"BackColor", RGB(255, 255, 255), iniFileName);
	hWndVar->hBitmap = GetPreviewBitmapW(hWndVar->fileName);
	if (hWndVar->hBitmap)
	{
		hWnd = CreateWindowExW(0, PLUGIN_NAME, L"", WS_CHILD | WS_VISIBLE, r.left, r.top, r.right - r.left, r.bottom - r.top, ParentWin, nullptr, hinst, (LPVOID)hWndVar);
		if (hWnd)
		{
			// update center image
			int ci = GetPrivateProfileIntW(SETTINGS, L"CenterImage", -1, iniFileName);
			switch (ci)
			{
			case 0:
			case 1:
				PostMessageW(ParentWin, WM_COMMAND, MAKELONG(ci, itm_center), (LPARAM)hWnd);
				WritePrivateProfileStringW(SETTINGS, L"CenterImage", nullptr, iniFileName);
				hWndVar->centerImage = ci ? TRUE : FALSE;
				break;

			default:
				hWndVar->centerImage = (ShowFlags & lcp_center) ? TRUE : FALSE;
				break;
			}

			SendMessageW(hWnd, WM_SET_SCROLLBAR, 0, 0);
			ShowWindow(hWnd, SW_SHOW);
			UpdateWindow(hWnd);
		}
		else
		{
			delete hWndVar;
			hWndVar = nullptr;
		}
	}

	return hWnd;
}

int __stdcall ListLoadNext(HWND ParentWin, HWND ListWin, char* FileToLoad, int ShowFlags)
{
	wchar_t FileToLoadW[wdirtypemax];
	return ListLoadNextW(ParentWin, ListWin, awfilenamecopy(FileToLoadW, FileToLoad), ShowFlags);
}

int __stdcall ListLoadNextW(HWND ParentWin, HWND ListWin, wchar_t* FileToLoad, int ShowFlags)
{
	int retVal = LISTPLUGIN_ERROR;
	if (ListWin)
	{
		auto hWndVar = reinterpret_cast<WNDVAR*>(GetWindowLongPtrW(ListWin, GWLP_USERDATA));
		if (hWndVar)
		{
			wcslcpy(hWndVar->fileName, FileToLoad, wdirtypemax);
			hWndVar->centerImage = (ShowFlags & lcp_center) ? TRUE : FALSE;
			hWndVar->fit2Window = (ShowFlags & lcp_fittowindow) ? TRUE : FALSE;
			hWndVar->fitLargerOnly = (ShowFlags & lcp_fitlargeronly) ? TRUE : FALSE;
			hWndVar->hScroll = FALSE;
			hWndVar->vScroll = FALSE;
			SecureZeroMemory(hWndVar->version, VERSTRMAXLENGTH*(sizeof(wchar_t)));
			if (hWndVar->hWndStatus)
			{
				// update status bar message
				if (GetFileVersionW(FileToLoad, hWndVar->version, VERSTRMAXLENGTH))
				{
					SendMessageW(hWndVar->hWndStatus, SB_SETTEXT, (WPARAM)0, (LPARAM)hWndVar->version);
				}
				else
				{
					SendMessageW(hWndVar->hWndStatus, SB_SETTEXT, (WPARAM)0, (LPARAM)L"");
				}
			}
			if (hWndVar->hBitmap)
			{
				DeleteObject(hWndVar->hBitmap);
			}
			hWndVar->hBitmap = GetPreviewBitmapW(hWndVar->fileName);
			if (hWndVar->hBitmap)
			{
				SendMessageW(ListWin, WM_SET_SCROLLBAR, 0, 0);
				if (InvalidateRect(ListWin, nullptr, FALSE))
				{
					retVal = LISTPLUGIN_OK;
				}
			}
		}
	}

	return retVal;
}

void __stdcall ListCloseWindow(HWND ListWin)
{
	if (ListWin)
	{
		auto hWndVar = reinterpret_cast<WNDVAR*>(GetWindowLongPtrW(ListWin, GWLP_USERDATA));
		DestroyWindow(ListWin);
		if (hWndVar)
		{
			delete hWndVar;
			hWndVar = nullptr;
		}
	}
}

HBITMAP __stdcall ListGetPreviewBitmap(char* FileToLoad, int width, int height, char* contentbuf, int contentbuflen)
{
	wchar_t FileToLoadW[wdirtypemax];
	return ListGetPreviewBitmapW(awfilenamecopy(FileToLoadW, FileToLoad), width, height, contentbuf, contentbuflen);
}

HBITMAP __stdcall ListGetPreviewBitmapW(wchar_t* FileToLoad, int width, int height, char* contentbuf, int contentbuflen)
{
	HBITMAP hBitmap = GetPreviewBitmapW((const wchar_t*)FileToLoad);
	BITMAP bm;
	ZeroMemory(&bm, sizeof(BITMAP));
	if (hBitmap && GetObjectW(hBitmap, sizeof(BITMAP), &bm))
	{
		int wSrc = bm.bmWidth;
		int hSrc = bm.bmHeight;
		// do we need to stretch?
		if ((wSrc >= width || hSrc >= height) && wSrc > 0 && hSrc > 0)
		{
			int wDst, hDst;
			int stretchy = MulDiv(width, hSrc, wSrc);
			if (stretchy <= height)
			{
				wDst = width;
				hDst = stretchy;
				if (hDst < 1)
				{
					hDst = 1;
				}
			}
			else
			{
				int stretchx = MulDiv(height, wSrc, hSrc);
				wDst = stretchx;
				if (wDst < 1)
				{
					wDst = 1;
				}
				hDst = height;
			}

			HWND hWnd = GetDesktopWindow();
			HDC hDC = GetDC(hWnd);
			if (hDC)
			{
				HDC hDCDst = CreateCompatibleDC(hDC);
				HDC hDCSrc = CreateCompatibleDC(hDC);
				HBITMAP hBitmapScaled = CreateCompatibleBitmap(hDC, wDst, hDst);
				ReleaseDC(hWnd, hDC);
				HBITMAP hOldBitmap = (HBITMAP)SelectObject(hDCSrc, hBitmap);
				HBITMAP hOldBitmapScaled = (HBITMAP)SelectObject(hDCDst, hBitmapScaled);
				SetStretchBltMode(hDCDst, HALFTONE);
				POINT pt;
				SetBrushOrgEx(hDCDst, 0, 0, &pt);
				StretchBlt(hDCDst, 0, 0, wDst, hDst, hDCSrc, 0, 0, wSrc, hSrc, SRCCOPY);
				SelectObject(hDCSrc, hOldBitmap);
				SelectObject(hDCDst, hOldBitmapScaled);
				DeleteDC(hDCSrc);
				DeleteDC(hDCDst);
				DeleteObject(hBitmap);
				hBitmap = hBitmapScaled;
			}
		}
	}

	return hBitmap;
}

int __stdcall ListPrint(HWND ListWin, char* FileToPrint, char* DefPrinter, int PrintFlags, RECT* Margins)
{
	wchar_t FileToPrintW[wdirtypemax];
	wchar_t DefPrinterW[wdirtypemax];
	return ListPrintW(ListWin, awfilenamecopy(FileToPrintW, FileToPrint), awfilenamecopy(DefPrinterW, DefPrinter), PrintFlags, Margins);
}

int __stdcall ListPrintW(HWND ListWin, wchar_t* FileToPrint, wchar_t* DefPrinter, int PrintFlags, RECT* Margins)
{
	int retVal = LISTPLUGIN_ERROR;

	if (ListWin)
	{
		HDC printer = nullptr;
		if (DefPrinter)
		{
			printer = CreateDCW(L"WINSPOOL", DefPrinter, nullptr, nullptr);
		}
		else
		{
			unsigned long defPrinterNameSize;
			GetDefaultPrinterW(nullptr, &defPrinterNameSize);
			wchar_t* DefPrinter2 = new wchar_t[defPrinterNameSize];
			if (DefPrinter2)
			{
				if (GetDefaultPrinterW(DefPrinter2, &defPrinterNameSize))
				{
					printer = CreateDCW(L"WINSPOOL", DefPrinter2, nullptr, nullptr);
				}
				else if (SET_DEBUG)
				{
					DisplayLastErrorMsgW(L"GetDefaultPrinterW", FileToPrint, MB_ICONSTOP | MB_OK);
				}
				delete[] DefPrinter2;
			}
		}
		if (printer)
		{
			auto hWndVar = reinterpret_cast<WNDVAR*>(GetWindowLongPtrW(ListWin, GWLP_USERDATA));
			if (hWndVar && hWndVar->hBitmap)
			{
				HDC hdcbitmap = CreateCompatibleDC(nullptr);
				if (hdcbitmap)
				{
					HBITMAP oldbitmap = (HBITMAP)SelectObject(hdcbitmap, hWndVar->hBitmap);
					if (oldbitmap)
					{
						BITMAP bm;
						ZeroMemory(&bm, sizeof(BITMAP));
						if (GetObjectW(hWndVar->hBitmap, sizeof(BITMAP), &bm))
						{
							double ratio = ((double)bm.bmWidth) / ((double)bm.bmHeight);
							int printedWidth = GetDeviceCaps(printer, PHYSICALWIDTH) - (Margins ? Margins->left : 0) - (Margins ? Margins->right : 0);
							int printedHeight = (int)((double)printedWidth / ratio);

							DOCINFOW di = { 0 };
							di.cbSize = sizeof(di);
							wchar_t wfp[_MAX_FNAME];
							wchar_t wep[_MAX_EXT];
							_wsplitpath(FileToPrint, nullptr, nullptr, wfp, wep);
							wchar_t FileToPrint2[wdirtypemax];
							wcslcpy(FileToPrint2, wfp, wdirtypemax);
							wcslcat(FileToPrint2, wep, wdirtypemax);
							di.lpszDocName = FileToPrint2;

							StartDocW(printer, &di);
							StartPage(printer);

							StretchBlt(printer, Margins ? Margins->left : 0, Margins ? Margins->top : 0, printedWidth, printedHeight, hdcbitmap, 0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY);

							EndPage(printer);
							EndDoc(printer);

							retVal = LISTPLUGIN_OK;

							SelectObject(hdcbitmap, oldbitmap);
						}
					}
					DeleteDC(hdcbitmap);
				}
			}
			DeleteDC(printer);
		}
		else if (SET_DEBUG)
		{
			DisplayLastErrorMsgW(L"CreateDCW", FileToPrint, MB_ICONSTOP | MB_OK);
		}
	}
	return retVal;
}

int __stdcall ListSendCommand(HWND ListWin, int Command, int Parameter)
{
	int retVal = LISTPLUGIN_ERROR;

	if (ListWin)
	{
		auto hWndVar = reinterpret_cast<WNDVAR*>(GetWindowLongPtrW(ListWin, GWLP_USERDATA));
		if (hWndVar && hWndVar->hBitmap)
		{
			switch (Command)
			{
			case lc_copy:
			{
				if (OpenClipboard(nullptr))
				{
					if (EmptyClipboard())
					{
						BITMAP bm;
						ZeroMemory(&bm, sizeof(BITMAP));
						HBITMAP hBitmapCopy = CopyBitmap(hWndVar->hBitmap);
						GetObjectW(hBitmapCopy, sizeof(BITMAP), &bm);
						if (SetClipboardData(CF_BITMAP, hBitmapCopy) != nullptr)
						{
							retVal = LISTPLUGIN_OK;
						}
						else if (SET_DEBUG)
						{
							DisplayLastErrorMsgW(L"SetClipboardData", hWndVar->fileName, MB_ICONWARNING | MB_OK);
						}

						if (!CloseClipboard())
						{
							if (SET_DEBUG)
							{
								DisplayLastErrorMsgW(L"CloseClipboard", hWndVar->fileName, MB_ICONWARNING | MB_OK);
							}
						}
					}
					else if (SET_DEBUG)
					{
						DisplayLastErrorMsgW(L"EmptyClipboard", hWndVar->fileName, MB_ICONWARNING | MB_OK);
					}
				}
				else if (SET_DEBUG)
				{
					DisplayLastErrorMsgW(L"OpenClipboard", hWndVar->fileName, MB_ICONWARNING | MB_OK);
				}

				break;
			}

			case lc_newparams:
			{
				hWndVar->centerImage = (Parameter & lcp_center) ? TRUE : FALSE;
				hWndVar->fit2Window = (Parameter & lcp_fittowindow) ? TRUE : FALSE;
				hWndVar->fitLargerOnly = (Parameter & lcp_fitlargeronly) ? TRUE : FALSE;
				SendMessageW(ListWin, WM_SET_SCROLLBAR, 0, 0);
				if (InvalidateRect(ListWin, nullptr, FALSE))
				{
					retVal = LISTPLUGIN_OK;
				}
				break;
			}
			}
		}
	}

	return retVal;
}

void __stdcall ListSetDefaultParams(ListDefaultParamStruct* dps)
{
	wchar_t Path[wdirtypemax];
	if (GetModuleFileNameW(hinst, Path, wdirtypemax) != 0)
	{
		awlcopy(iniFileName, dps->DefaultIniName, wdirtypemax);
		wchar_t wdd[_MAX_DRIVE];
		wchar_t wpd[_MAX_PATH];
		wchar_t wdp[_MAX_DRIVE];
		wchar_t wpp[_MAX_PATH];
		wchar_t wfp[_MAX_FNAME];
		_wsplitpath_s(iniFileName, wdd, _MAX_DRIVE, wpd, _MAX_PATH, nullptr, 0, nullptr, 0);
		_wsplitpath_s(Path, wdp, _MAX_DRIVE, wpp, _MAX_PATH, wfp, _MAX_FNAME, nullptr, 0);
		wchar_t iniFileNameLookFirst[wdirtypemax] = L"";
		wcslcpy(iniFileNameLookFirst, wdp, wdirtypemax);
		wcslcat(iniFileNameLookFirst, wpp, wdirtypemax);
		wcslcat(iniFileNameLookFirst, wfp, wdirtypemax);
		wcslcat(iniFileNameLookFirst, L".ini", wdirtypemax);

		// see if the INI file already exists in the plugin directory
		WIN32_FIND_DATAW findData;
		ZeroMemory(&findData, sizeof(WIN32_FIND_DATAW));
		if (FindFirstFileW(iniFileNameLookFirst, &findData) != INVALID_HANDLE_VALUE)
		{
			wcslcpy(iniFileName, iniFileNameLookFirst, wdirtypemax);
		}
		else
		{
			wcslcpy(iniFileName, wdd, wdirtypemax);
			wcslcat(iniFileName, wpd, wdirtypemax);
			wcslcat(iniFileName, wfp, wdirtypemax);
			wcslcat(iniFileName, L".ini", wdirtypemax);
		}

		// see if the INI file already exists
		SecureZeroMemory(&findData, sizeof(WIN32_FIND_DATAW));
		if (FindFirstFileW(iniFileName, &findData) == INVALID_HANDLE_VALUE)
		{
			// load default INI file string from resource
			HRSRC hRes = FindResourceW((HMODULE)hinst, MAKEINTRESOURCEW(IDR_INIFILE), L"FILE");
			HGLOBAL hGlobal = LoadResource((HMODULE)hinst, hRes);
			DWORD dwBytesToWrite = SizeofResource((HMODULE)hinst, hRes);
			DWORD dwBytesWritten = 0;
			BOOL err = FALSE;
			const char* iniStr = static_cast<const char*>(LockResource(hGlobal));
			if (iniStr)
			{
				HANDLE hFile = CreateFileW(iniFileName, GENERIC_WRITE, 0, nullptr, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, nullptr);
				if (hFile == INVALID_HANDLE_VALUE)
				{
					DisplayLastErrorMsgW(L"CreateFileW", iniFileName, MB_ICONWARNING | MB_OK);
				}
				else
				{
					while (dwBytesWritten < dwBytesToWrite)
					{
						if (FALSE == WriteFile(hFile, iniStr + dwBytesWritten, dwBytesToWrite - dwBytesWritten, &dwBytesWritten, nullptr))
						{
							DisplayLastErrorMsgW(L"WriteFile", iniFileName, MB_ICONWARNING | MB_OK);
							err = TRUE;
							break;
						}
					}
					CloseHandle(hFile);
				}
			}
			UnlockResource(hGlobal);
			FreeResource(hRes);
			if (err == FALSE)
			{
				wchar_t msg[wdirtypemax + 100];
				_snwprintf(msg, wdirtypemax + 99, L"New initialization file is \"%s\"", iniFileName);
				MessageBoxW(nullptr, msg, PLUGIN, MB_ICONINFORMATION | MB_OK);
			}
		}
	}
}
